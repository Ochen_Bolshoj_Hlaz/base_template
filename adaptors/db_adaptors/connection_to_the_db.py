import psycopg2


def connect_to_the_database(data_to_connect):
    connection = psycopg2.connect(host=data_to_connect['database_server_ip'],
                                  user=data_to_connect['database_user'],
                                  password=data_to_connect['database_password'],
                                  dbname=data_to_connect['database_name'],
                                  port=data_to_connect['database_port'])
    connection.autocommit = True

    return connection


def disconnect_to_the_database(connection):
    connection.close()
