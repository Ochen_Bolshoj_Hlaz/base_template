from adaptors.db_adaptors.connection_to_the_db import connect_to_the_database, disconnect_to_the_database
from settings.settings import PARAMETERS_OF_CONNECTION_TO_THE_APPLICATION_DB


class QueryProcessorForDB(object):

    @classmethod
    def access_to_the_database(cls, query, type_select=None):
        connect = connect_to_the_database(PARAMETERS_OF_CONNECTION_TO_THE_APPLICATION_DB)
        cursor = connect.cursor()
        cursor.execute(query)
        connect.commit()
        if type_select:
            return cursor.fetchall()

        disconnect_to_the_database(connect)
        return cursor

    def insert_the_database(self, query):
        self.access_to_the_database(query)

    def select_the_database(self, query):
        return self.access_to_the_database(query, True)

    def delete_data_the_database(self, query):
        self.access_to_the_database(query)

    def truncate_table_cascade_the_database(self, query):
        self.access_to_the_database(query)
