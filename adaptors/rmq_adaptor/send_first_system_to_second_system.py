import pika
import json
from settings.settings import PARAMETERS_OF_CONNECTION_TO_THE_RABBIT as data


def connection_to_the_chanel_in_rabbit():
    crd = pika.PlainCredentials(data['login'], data['password'])
    param = pika.ConnectionParameters(
        host=data['host'],
        port=data['port'],
        credentials=crd,
        virtual_host=data['virtual_host'])
    connection = pika.BlockingConnection(param)
    return connection.channel()


def close_connection(connection):
    connection.close()


def send_messages(body):
    connection_channel = connection_to_the_chanel_in_rabbit()

    connection_channel.queue_declare(queue=data['queue'], durable=True)
    connection_channel.basic_publish(exchange=data['exchange'], routing_key=data['routing_key'], body=body)

    close_connection(connection_channel)


def send_first_system_to_second_system(response_body):
    if len(response_body) > 1:
        send_messages(json.dumps(response_body))
        print("send_messages", response_body)
    else:
        for item in response_body:
            send_messages(json.dumps(item))
            print("send_messages", item)

# пример сообщения от монализы к симпоме
    send_first_system_to_second_system({
        "datetime_sent": "2019-01-30 00:00:01.000000+00:00",
        "title": "Закон подлостиqweqweqwe.",
        "url": "https://pikabu.ru/story/zakon_podlosti_61172452123123",
        "detection_datetime": "2019-01-30T14:00:01.000000+00:00",
        "is_datetime_parsed_correctly": "True",
        "material_type": 1,
        "publication_datetime_article": "2019-01-30T14:00:01+00:00",
        "author_article": "Попадает в Симпому - Danik007",
        "author_comment": "",
        "datetime_simpoma": "2019-01-30 00:00:01.000000+00:00",
        "text": "Закон подлости.qwe Я: я начал встречаться с одной классной девушкой. Парикмахер: давно? Я:",
        "source_id": "3758",
        "publication_datetime_comment": "",
        "external_id": 124548637123123,
        "high_level_tags": "{\"priznaki_narusheniy_necenzurnaya_bran_mat\": \"начал, девушкой\"}"
    })

