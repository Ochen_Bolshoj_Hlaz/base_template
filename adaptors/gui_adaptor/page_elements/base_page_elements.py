from adaptors.gui_adaptor.locators.base_page_locators import BasePageLocators as Locators


class BasePageElements(object):

    def __init__(self, driver):
        self.driver = driver

    def get_some_element(self):
        return self.driver.find_element(*Locators.SOME_BASE_LOCATOR)
