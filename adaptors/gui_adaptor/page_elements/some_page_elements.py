from adaptors.gui_adaptor.locators.some_page_locators import SomePageLocators as Locators


class SomePageElements(object):

    def __init__(self, driver):
        self.driver = driver

    def get_some_element(self):
        return self.driver.find_element(*Locators.SOME_LOCATOR)
