УРОВЕНЬ ЭЛЕМЕНТОВ

* base_page_elements.py 
> в модуле находится функционал для поиска элемента/элементов
> на веб странице. Эти элементы должны повторяться на разных страницах. 
> Например, инпут используемый для поиска
> находится на странице1 и на странице2 и при этом имеет одинаковый элемент.

> ```self.driver.find_element(*Locators.SOME_BASE_LOCATOR)```

> более подробнее [здесь](https://selenium-python.readthedocs.io/locating-elements.html#)

> так же можно написать поиск элемента в виде
 
> ```WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located(Locators.USER_LABEL))```

> такая конструкция необходима, когда нужно ждать, что элемент появится(обычно используется для проверок) пример [здесь](https://selenium-python.readthedocs.io/waits.html#explicit-waits) 


* some_page_elements.py 
> модуль в котором описываются элементы, которые
> используются только на одной странице. имя модуля даётся по функциональности той страницы которая описыватеся. 
> Например: authorization_page_elements.py, модуль с описанием элементов, которые относятся к странице с авторизацией.
> можно так же использовать обе конструкции описанные выше для поиска элементов

* ```__init__```
>  используется для инициализации драйвера, в нашем случае хромдрайвера

*  имя класса должно быть логически понятным и описывать тестируемую страницу на английском языке

* ```from adaptors.gui_adaptor.locators.login_page_locators import LoginPageLocators as Locators```
> подключение локаторов которые относятся логически к тестируемой странице и задаётся маленькое имя для класса локаторов
> чтобы строчки были не большими и понятными