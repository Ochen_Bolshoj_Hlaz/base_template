from adaptors.gui_adaptor.page_elements.some_page_elements import SomePageElements


class SomePageObjects(object):

    def __init__(self, driver):
        self.driver = driver
        self.elements = SomePageElements(self.driver)

    def action_some_element(self):
        return self.elements.get_some_element().click()
