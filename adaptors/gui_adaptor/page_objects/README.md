 УРОВЕНЬ ДЕЙСТВИЙ НАД ЭЛЕМЕНТАМИ
 
 * base_page_objects.py 
> модуль который предостовляет возможность совершать различные действия
> например клик, текст, is_displayed, send_keys, получение атрибутов и др.

> ```self.elements.get_some_element().click()```

> Более подробная инфа о действиях [здесь](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.clear)

* some_page_objects.py
> модуль в котором описываются действия которые используются только на одной странице.

* ```__init__``` 
> используется для инициализации драйвера, в нашем случае хромдрайвера
> используется для инициализации элементов, над которыми будут совершаться действия.
 
* from adaptors.gui_adaptor.page_elements.some_page_elements import SomePageElements
> подключение элементов
 