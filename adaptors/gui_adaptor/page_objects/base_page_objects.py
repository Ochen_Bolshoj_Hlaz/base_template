from adaptors.gui_adaptor.page_elements.base_page_elements import BasePageElements


class BasePageObjects(object):

    def __init__(self, driver):
        self.driver = driver
        self.base_page_elements = BasePageElements(self.driver)

    def action_some_element(self):
        return self.base_page_elements.get_some_element().click()
