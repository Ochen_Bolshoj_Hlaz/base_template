CHROME_DRIVER_PATH = '/usr/local/bin/chromedriver'

BASE_IP = '192.168.71.62'
BASE_URL = 'http://{0}/'.format(BASE_IP)
PARAMETERS_OF_CONNECTION_TO_THE_APPLICATION_DB = {'database_server_ip': BASE_IP,
                                                  'database_port': 5432,
                                                  'database_name': 'expert',
                                                  'database_user': 'expert',
                                                  'database_password': 'expert123'}

PARAMETERS_OF_CONNECTION_TO_THE_RABBIT = {
    'login': 'admin',
    'password': 'admin',
    'host': '192.168.71.56',
    'port': 5672,
    'virtual_host': 'simpoma',
    'queue': 'liza_simpoma_boy',
    'exchange': 'simpoma',
    'routing_key': 'liza_simpoma_boy'
}

PREFIX_URL_API = '#/'
BASE_IP_API = BASE_URL + PREFIX_URL_API
