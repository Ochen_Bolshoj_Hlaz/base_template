import allure

from library.steps.init_steps import default_setup, default_teardown
from library.steps.some_steps import SomeSteps


@allure.feature('Тестируемая страница')
class TestSomePage(object):

    def setup(self):
        self.driver = default_setup(False)
        self.steps = SomeSteps(self.driver)

    @allure.title("Описание тестируемого кейса")
    def test_some_case(self):
        self.steps.some_step1()
        self.steps.some_step2()
        self.steps.some_check_step()

    def teardown(self):
        default_teardown(self.driver)
