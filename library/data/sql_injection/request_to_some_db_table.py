from psycopg2._psycopg import IntegrityError
from library.data.const.const_some_db_data import *
from library.data.const.const_query_string_in_database import INSERT_SOME_TABLE
from adaptors.db_adaptors.query_processor_for_bd import QueryProcessorForDB


class AddSomeTable(object):

    def __init__(self):
        self.table = SOME_TABLE_IN_DB

    def add_value_in_db(self):
        raw_text_request = INSERT_SOME_TABLE

        for item in self.table:
            try:
                QueryProcessorForDB().insert_the_database(raw_text_request.format(
                    item['value1'], item['value2'], item['value3'], item['value4']))
            except IntegrityError:
                pass
