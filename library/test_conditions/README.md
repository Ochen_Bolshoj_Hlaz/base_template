* base_assertions.py 
> модуль, в котором описаны методы для проверок на 
> основе библиотеки assertpy.

* check_that_the_element_text_matches 
> метод сравнивающий на эквивалентность
> два текста. Принимает два параметра: should_be_text и received_text. 
> Вовзращается или True или False

* check_that_element_start_with 
> метод проверяющий начинается ли текст
> с нужных нам слов. should_be text(начало текста) received_text(весь текст) возвращается true или false

* check_that_element_contains_in 
> метод проверяющий содержит ли текст нужную нам последовательность символов

* check_that_element_does_not_contains_in 
> метод проверяющий, что нужная нам последовательность симолов не содержится в проверяемом тексте

* check_that_true_match 
> метод который возвращает true если передаваемое уловие верно

* check_that_false_match 
> метод который возвращает true если возвращаемое условие не верно

* checking_two_lists_for_equivalence 
> метод проверяющий эквивалентность 
> двух массивов

* check_that_string_is_not_empty 
> метод проверяющий строку на не пустоту,
> возвращает true если передаваемая строка не пустая

* check_that_string_is_empty 
> метод проверяющий строку на пустоту,
> вовзращает true если передаваемая строка пустая
