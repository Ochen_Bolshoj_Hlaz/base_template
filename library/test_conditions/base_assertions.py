from assertpy import assert_that


class BaseAssertions(object):

    @staticmethod
    def check_that_the_element_text_matches(should_be_text, received_text):
        assert_that(received_text).is_equal_to(should_be_text)

    @staticmethod
    def check_that_element_start_with(should_be_text, received_text):
        assert_that(received_text).starts_with(should_be_text)

    @staticmethod
    def check_that_element_contains_in(should_be_text, received_text):
        assert_that(received_text).contains(should_be_text)

    @staticmethod
    def check_that_element_does_not_contains_in(should_be_text, received_text):
        assert_that(received_text).does_not_contain(should_be_text)

    @staticmethod
    def check_that_true_match(element):
        assert_that(element).is_true()

    @staticmethod
    def check_that_false_match(element):
        assert_that(element).is_false()

    @staticmethod
    def checking_two_lists_for_equivalence(first_list, second_list):
        assert_that(first_list) \
            .is_length(len(second_list)) \
            .is_equal_to(second_list) \
            .starts_with(second_list[0]) \
            .ends_with(second_list[-1])

    @staticmethod
    def check_that_string_is_not_empty(string):
        assert_that(string).is_not_empty()

    @staticmethod
    def check_that_string_is_empty(string):
        assert_that(string).is_empty()
