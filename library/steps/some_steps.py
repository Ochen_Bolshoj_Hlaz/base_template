import allure

from adaptors.gui_adaptor.page_objects.some_page import SomePageObjects
from library.test_conditions.base_assertions import BaseAssertions


class SomeSteps(object):

    def __init__(self, driver):
        self.driver = driver
        self.object = SomePageObjects(self.driver)
        self.assertions = BaseAssertions()

    @allure.step('Описание шага1')
    def some_step1(self):
        self.object.click_some_object()

    @allure.step('Описание шага2')
    def some_step2(self):
        self.object.some_step2()

    @allure.step('Описание проверки')
    def some_check_step(self):
        self.assertions.some_assertion()
