import allure

from adaptors.gui_adaptor.page_objects.base_page_objects import BasePageObjects
from library.test_conditions.base_assertions import BaseAssertions


class BasePageSteps(object):

    def __init__(self, driver):
        self.driver = driver
        self.assertions = BaseAssertions()
        self.base_page_object = BasePageObjects(self.driver)

    @allure.step("Описание шага")
    def action_some_element(self):
        self.base_page_object.action_some_element()
