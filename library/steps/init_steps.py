import allure

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from settings.settings import CHROME_DRIVER_PATH, BASE_URL
from adaptors.db_adaptors.query_processor_for_bd import QueryProcessorForDB
from library.data.const.const_query_string_in_database import QUERY_TRUNCATE_CASCADE


@allure.step('Инициализация браузера')
def default_setup():
    truncate_data_in_db()
    options = chrome_options()
    driver = webdriver.Chrome(chrome_options=options, executable_path=CHROME_DRIVER_PATH)
    driver.implicitly_wait(10)
    driver.get(BASE_URL)

    return driver


@allure.step('Очистка базы данных')
def truncate_data_in_db():
    QueryProcessorForDB().truncate_table_cascade_the_database(QUERY_TRUNCATE_CASCADE)


@allure.step('Установка опций хрома')
def chrome_options():
    options = Options()
    # options.add_argument("--headless")
    options.add_argument('--incognito')
    options.add_argument('--start-maximized')
    return options


def default_teardown(driver):
    driver.quit()
